# Ejercicio 1
# Context
# Recent growth at your at your organization has made local-level server loggin
# untenable and difficult to manage. Address this bay doing the following:

# Task:
# Install the rsyslog server on node exam-1, and configure it to listen on port
# 3514/UDP. Ensure that the solution is configured to
# start automatically if the exam-1 node is rebooted

ssh exam-1
sudo apt-get install rsyslog
sudo systemctl start rsyslog
sudo systemctl enable rsyslog
sudo systemctl status rsyslog

sudo mkdir /var/log/remote_logs
sudo chown syslog:syslog -R /var/log/remote_logs

sudo vim /etc/rsyslog.conf

# descomentar
$ModLoad imuxsock
$ModLoad imjournal

# descomentar
$ModLoad imudp
$UDPServerRun 514

# modificar
$UDPServerRun 514
# por
$UDPServerRun 3514

#agregar reglas para procesar el log remoto asi
$template RemoteLogs,"/var/log/remote_logs/%HOSTNAME%_%PROGRAMNAME%.log"
*.* ?RemoteLogs
& ~

# o asi

$template remote-incoming-logs,"/var/log/remote_logs/%HOSTNAME%_%PROGRAMNAME%.log"
*.* ?remote-incoming-logs
& ~

sudo systemctl restart rsyslog

sudo ss -tulnp | grep "rsyslog"

# Ejercicio 2
# Context
# Working with columes is an importan engineering skill.
# Perform the following to demostrate your skills:

# Task
# 1. Create a colume-group named vg2 on disk
# /dev/xvdt1 /dev/xvdt2 /dev/xvdt3
# with  physical extent size of 5MB

sudo vgcreate vg2 --physicalextentsize 5MB /dev/xvdt1 /dev/xvdt2 /dev/xvdt3
# o tambien
sudo vgcreate vg2 -s 5MB /dev/xvdt1 /dev/xvdt2 /dev/xvdt3

# 2. Create a striped volume striped volumestripedVol2
# on vg2 with size 30MB with stripe-size 32MB

sudo lvcreate -L 30MB -I 32M -n volumestripedVol2 vg2

# 3. Create a mirrored volume mirroredVol3 on vg1
# with size 32MB

sudo lvcreate -L 32Mb -m1 -n mirroredVol3 vg1

# 4. Extend the size of existing volume linearVol by 12MB
sudo lvextend -L12G /dev/vg1/linearVol

# Ejercicio 3
# To allow a user on a remote system access to an
# unprivilege process on a standard port, redirect
# incoming traffic for interface nat1 on port 443/TCP
# to port 22022 persistently on node net-exam
ssh net-exam-1
sudo apt install -y iptables-save
sudo apt install -y iptables-persistent
sysctl net.ipv4.ip_forward=1
#para hacerlo persistente vim /etc/sysctl.conf y buscar net.ipv4.ip_forward=1
sudo iptables -A PREROUTING -t nat -i nat1 -p tcp --dport 443 -j REDIRECT --to-port 22022
sudo iptables-save > /etc/iptables/rules.v4

# Ejercicio 4
# Context
# Your engineering team needs a new test LXC container
# to be created from an existing production application

# Task
# 1. Clone the container FinanceFE to a container named
# testly that does not launch at boot

# 2. Both the FinanceFE source container, and the testly
# clone, should use a root container path of /srv/lxc

# para los dos consignas
lxc-stop FinanceFE
lxc-copy -n FinanceFE -N testly -p /srv/lxc
lxc-start FinanceFE
lxc-start testly

# Ejercicio 5
# Context
# Your organization is in the process of launching a
# new brand. Part of the work required for this launch involves the
# configuration of a master DNS server for the new domain
# name which has been registered. To provide access to a
# new webserver both internally and externally, you will make
# use of BIND views to provide clients with access to two different
# zonesfiles for the example.net domain

# task
# Perform the following tasks to install an congirue the master DNS server on node
# exam-1

# 1. Install BIND on the server, and ensure it is started end configured to start
# automatically on boot. BIND must be listening on at least the loopback interface
# 2. Create the /etc/bind/zones directory
# 3. Create an RFC 1035 complian zonefile for the example.net domain,
# to be served to internal clients. The zone file must be placed at
# /etc/bind/zones/example.net-internal.db
# Ensure that an A record exist for the new web server for the new webserver
# with a host label of webdev.example.net and record data od 192.168.23.85.
# The ttl is not important
# 4. Create an RFC 1035 complian zonefile for the example.net domain,
# to be served to external clients. The zone file must be placed at
# /etc/bind/zones/example.net-external.db
# Ensure that an A record exist for the new web server for the new webserver
# with a host label of webdev.example.net and record data of 203.0.133.185.
# The ttl is not important
# 5. Configure BIND using the view mechanis so that the internal copy of the zone
# is served to clients whose request originate from subnet 10.250.0.0/24 only, and
# the external copy of the zone is served to all other clients
# 6. Ensure that bind is reloaded appropiately so that your changes are made live



sudo apt-get update && apt-get install bind9 bind9utils
sudo cp /etc/bind/named.conf /etc/bind/named.conf.orig
sudo mkdir /etc/bind/zones

#### comentar el archivo /etc/bind/named.conf.options

sudo nano /etc/bind/name.conf
contenido del archivo
options
{
        directory "/etc/bind/zones";
        listen-on { any; };
        allow-query { any; };
        recursion yes;
        dnssec-enable no;
        dnssec-validation no;
        dnssec-lookaside auto;
        forwarders {
                8.8.8.8;
                8.8.4.4;
        };
};

zone "allenjagger.net" IN { # ---> cambiar por example
        type master;
        file "/etc/bind/zones/allenjagger.net.zone";
};

zone "0.168.192.in-addr.arpa" IN { # ---> nombre por de facto
        type master;
        file "/etc/bind/zones/0.162.198.in-addr.arpa.zone";
};

zone "allenjagger.net-external" IN { # ---> cambiar por example
        type master;
        file "/etc/bind/zones/allenjagger.net-external.zone";
};

zone "133.0.203.in-addr.arpa.zone" IN { # ---> nombre por de facto
        type master;
        file "/etc/bind/zones/133.0.203.in-addr.arpa.zone";
};

# configuracion de views
acl interna { 192.168.100.0/24; localhost; };
acl externa { 203.0.133.0/24; };
view todas {
    match-clients { interna; externa;};
};

view interna {
    match-clients { interna; };
    allow-recursion { any; };

        zone "allenjagger.net" { # ---> cambiar por example
                type master;
                file "/etc/bind/zones/allenjagger.net.zone";
        };

        zone "0.168.192.in-addr.arpa" { # ---> nombre por de facto
                type master;
                file "/etc/bind/zones/0.162.198.in-addr.arpa.zone";
        };
        # include "/etc/bind/zones.rfc1918";
        # include "/etc/bind/named.conf.default-zones";
};

view externa {
    match-clients { externa; };
    allow-recursion { any; };

        zone "allenjagger.net-external" IN { # ---> cambiar por example
                type master;
                file "/etc/bind/zones/allenjagger.net-external.zone";
        };

        zone "133.0.203.in-addr.arpa.zone" IN { # ---> nombre por de facto
                type master;
                file "/etc/bind/zones/133.0.203.in-addr.arpa.zone";
        };
        # include "/etc/bind/zones.rfc1918";
        # include "/etc/bind/named.conf.default-zones";
};


named-checkconf /etc/named.conf

chown -R bind:bind /etc/bind/zones
cd /etc/bind/zones
touch allenjagger.net.zone
#contenido del archivo allenjagger.net.zone

$TTL    604800

@       IN      SOA     allenjagger.net. root.allenjagger.net. (
        2016051101 ; Serial
        10800 ; Refresh
        3600 ; Retry
        604800 ; Expire
        604800) ; Ngative TTL
;
@       IN      NS      dns.allenjagger.net.
dns     IN      A       192.168.100.207
web1    IN      A       192.168.100.208

named-checkzone allenjagger.net allenjagger.net.zone

touch 0.162.198.in-addr.arpa.zone
#contenido 0.162.198.in-addr.arpa.zone
$TTL    604800

@       IN      SOA     allenjagger.net. root.allenjagger.net. (
        2016051101 ; Serial
        10800 ; Refresh
        3600 ; Retry
        604800 ; Expire
        604800) ; Minimun TTL
;
@       IN      NS      dns.allenjagger.net.
208     IN      PTR     web1.allenjagger.net.

named-checkzone 0.162.198.in-addr.arpa 0.162.198.in-addr.arpa.zone

touch allenjagger.net-external.zone
#contenido del archivo allenjagger.net-external.zone

$TTL    604800

@       IN      SOA     allenjagger.net. root.allenjagger.net. (
        2016051101 ; Serial
        10800 ; Refresh
        3600 ; Retry
        604800 ; Expire
        604800) ; Ngative TTL
;
@       IN      NS      dns.allenjagger.net.
dns     IN      A       203.0.133.185  # ---> necesito otra para el dns
web1    IN      A       203.0.133.185

touch 133.0.203.in-addr.arpa.zone
#contenido del archivo 133.0.203.in-addr.arpa.zone
$TTL    604800

@       IN      SOA     allenjagger.net. root.allenjagger.net. (
        2016051101 ; Serial
        10800 ; Refresh
        3600 ; Retry
        604800 ; Expire
        604800) ; Minimun TTL
;
@       IN      NS      dns.allenjagger.net.
185     IN      PTR     web1.allenjagger.net.

systemctl restart named
systemctl enable named

# agregar a /etc/network/interfaces
dns-nameservers 192.168.100.207

host web1.allenjagger.net
host 192.168.100.208

# Ejercicio 6
# You have been instructed to share a directory from server exam-1 via
# via NFSv4 and have been given the following requeriments:
# 1 Create a NFSv4 share on host exam-1 to provide access to the
# /nfs/share3 directory
# 2 Share this directory with the subnet 10.1.5.0/24 as a read only,
# insecure share
# 3 Share this directory with the domain example.org as a writeable,
# secure share. Ensure that the appropiate export option is set so
# that the root UID/GID is mapped to the anonymous UID/GID
# 4 Set up the share to ensure that files are created with default
# permissions of 644, and directories with defauls permissions 775

sudo apt update
sudo apt install nfs-kernel-server
sudo mkdir /nfs/share3
sudo chown nobody:nogroup /nfs/share3
sudo nano /etc/exports

#contenido del archivo
/nfs/share3 10.1.5.0/24(ro,insecure)
/nfs/share3 example.org(rw,secure)

#contenido de /etc/idmapd.conf ---> importante
[Mapping]

Nobody-User = nobody
Nobody-Group = nogroup
#seteando permisos
setfacl -d -R --set u::rwx,g::rwx,o::r-x /nfs/share3

#aplicando cambios
sudo exportfs -rav
sudo systemctl restart nfs-kernel-server

sudo iptables -A INPUT -p tcp -m tcp --dport 111 -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp --dport 2049 -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp --dport 20048 -j ACCEPT
sudo iptables -A INPUT -p udp -m udp --dport 111 -j ACCEPT
sudo iptables -A INPUT -p udp -m udp --dport 2049 -j ACCEPT
sudo iptables -A INPUT -p udp -m udp --dport 20048 -j ACCEPT

#o bien
sudo ufw allow from 10.1.5.0/24 to 2049 nfs

# Ejercicio 7
# To protect your network from unsolicited acces from your DMZ machine,
# configure the node net-exam-3 to allow new connections from the host
# obtainer by runnin getnet hosts nex-exam-1 on port 6789/TCP but disallow
# any other acess from or to host net-exam-1
getent hosts net-exam-1

iptables -A INPUT -s 192.168.10.2 -p tcp --dport 6789 -m state --state NEW -j ACCEPT
iptables -A INPUT -p tcp -m state --state NEW -j DROP
iptables -A INPUT -p udp -j DROP
iptables -A FORWARD -s 192.168.10.2 -p tcp --dport 6789 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp -m state --state NEW -j DROP
iptables -A FORWARD -p udp -j DROP
iptables -A OUTPUT -d 192.168.10.2 -p tcp -m state --state NEW -j DROP
iptables -A OUTPUT -d 192.168.10.2 -p udp -j DROP

sudo apt install -y iptables-save
sudo apt install -y iptables-persistent
sudo iptables-save > /etc/iptables/rules.v4

#Ejercicio 8
# Context
# you have been instructed to configure a remote server node-2 to
# optimize performance for a database applicationdue to be
# installed by the DBA responsible for this system

# 1. Ensure the remote server is set to 12 hugepages
sudo sysctl -w vm.nr_hugepages=12
sudo bash -c "echo "vm.nr_hugepages=12" >> /etc/sysctl.conf"
# 2. Set the overcommit memmory mode to 1
sudo sysctl -w vm.overcommit_memory=1
sudo bash -c "echo "vm.overcommit_memory=1" >> /etc/sysctl.conf"
# 3. Set the memmory swappiness to 45
sudo sysctl -w vm.swappiness = 45
sudo sysctl -w vm.overcommit_ratio=45
sudo bash -c "echo "vm.swappiness = 45" >> /etc/sysctl.conf"
sudo bash -c "echo "vm.overcommit_ratio=45" >> /etc/sysctl.conf"
# 4. Set the group with 202 to be alllowed to create hugepages
sudo sysctl -w vm.hugetlb_shm_group=202
sudo bash -c "echo "vm.hugetlb_shm_group=202" >> /etc/sysctl.conf"
# 5. All changes shoul be persistend after reboot

# Ejercicio 9
# The operations manager has given you a task to suppor the
# the development team's new project.

# Task
# 1. Create a new libvirt-lxc managed domain using the templete provided
# at /opt/CEDD00501/domain.xml, and modifying it accordingly
# 2. This is an LXC domain, ant that aspect of the configuration must not be changed
# 3. The domain must be set to autostart
# 4. Domain specs:
#       - Name: cloudx-dev
#       - Memory: 5112MB
#       - Virtual CPUs: 1
#       - Network: br0

<domain type='lxc'>
  <name>cloudx-dev</name>
  <os>
    <type>exe</type>
    <init>/bin/sh</init>
  </os>
  <vcpu>1</vcpu>
  <memory unit='MiB'>512</memory>
  <devices>
    <interface type='br0'>
	    <source network='br0' />
    </interface>
    <console type='pty' />
  </devices>
</domain>
# revisar si esta creado la red br0 con
sudo virsh net-list --all
# si no esta creada copiar de dump haciendo
sudo virsh net-dumpxml default >> br0.xml
sudo virsh net-destroy default
# modificar br0.xml de esta forma, cambiar nombre de puente, direccion mac, ip y los rangos ip segun la ip

<network connections='1'>
  <name>br0</name>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='virbr1' stp='on' delay='0'/>
  <mac address='52:54:00:57:4b:3a'/>
  <ip address='192.168.123.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.123.2' end='192.168.123.254'/>
    </dhcp>
  </ip>
</network>

sudo virsh net-create br0.xml
sudo virsh -c lxc:/// define domain.xml
sudo virsh -c lxc:/// autostart cloudx-dev
sudo virsh -c lxc:/// start cloudx-dev

# Ejercicio 10
# Context
# Fixing issues with the system's mandatory access control are
# common when these framworks are active
# Task
# The MariaDB server on this system is unable to start so please
# perform the following:
# 1. Diagnose and fix the issue
# 2. When you have fixed the problem, leave the MariaDB server
# running
# 3. You must not change the MariaDB configuration, nor should you
# disable the system's mandatory access control system or place
# it in permissive/complaining mode, or remove/disable any specific
# policies

service mysqld stop
/etc/init.d/mysqld stop
mysqladmin -u root -p shutdown

pkill -9 mysqld
# o bien
ps aux | grep mysql
kill -9 4969 # --> siendo que el nombre del proceso sea 4969

#buscar
find . -name mysqld.sock
# o bien
sudo find / -type s

cuando lo encuentre, lo copiamos en
/var/run/mysqld/
o hacemos un ln -s
ln -s ruta_del_archivo /var/run/mysqld/mysqld.sock

# Ejercicio 11
# Context
# The website you currently manage has been extended to include an SSL secure
# application extension. You have been assigned the task of installing a webserver
# on this system and configuring the SSL properties according to your  organization's
# security policy

# Task
# 1. Install a webserver and set it to listen on port 899 for HTTP and
# 8993 for HTTPS traffic
# 2. Using the prod_pk.key private key file in /opt/CESC00101/ssl, generate a
# Certificate Signing Request named /opt/CESC00101/ssl/ca.csr with the following values:
#         - Country Code: TT
#         - State or Province: Far East
#         - City: Elysian
#         - Organization name: Coretech
#         - OU Name: MGMT
#         - Server Name: store.example.com
#         - Email address: nikito@example.org
# 3. Generate a self-signed certificate named ca.crt in /opt/CESC00101/ssl, using the
# CSR you just generated and the prod_pk.key private key gile, both of which are located in
# /opt/CESC00101/ssl. The certificate should expire in 365 days
# 4. Copy the files to the following locations:
        # - ca: /etc/pki/tls/certs/
        # - Key: /etc/pki/tls/private/
        # - Certificate Signing Request: /etc/pki/tls/private/
# 5. Update the webserver configuration file to include the location
# of the certificate file and the certificate key
# 6. The webserver should be started, and configured to automatically start on boot

#1
sudo apt-get install nginx
sudo nano /etc/nginx/sites-available/default
#cambiar puerto solo http, no cambiar https hasta no configurar los certificados
# como generar un par de claves openssl req -new -newkey rsa:2048 -nodes -keyout prod_pk.key -out ca.csr si no tengo par de claves
#2
openssl req -out ca.csr -key prod_pk.key -new
#3
openssl x509 -req -days 365 -in ca.csr -signkey prod_pk.key -sha256 -out ca.crt
#4
cd /etc/pki/tls/certs/
cp /opt/CESC00101/ssl/ca.crt ca.crt

cd /etc/pki/tls/private/
cp /opt/CESC00101/ssl/prod_pk.key prod_pk.key
cp /opt/CESC00101/ssl/ca.csr ca.csr
#5
#asi tiene que quedar el archivo
sudo nano /etc/nginx/sites-available/default
server {
        listen 899 default_server;
        listen [::]:899 default_server;

        # SSL configuration
        #
        listen 8993 ssl default_server;
        listen [::]:8993 ssl default_server;
        ssl_certificate /etc/pki/tls/certs/ca.crt;
        ssl_certificate_key /etc/pki/tls/private/prod_pk.key;
        #
#6
sudo ufw allow 899/tcp
sudo ufw allow 8993/tcp
systemctl stop nginx.service
systemctl start nginx.service
systemctl enable nginx.service

# Ejercicio 12
# System administratos and systems engineers
# are frequently tasked with
# manipulating text files, whether they contain
# configuration, data, or comprise
# script or programming source code
# Task
# Complete the following task to demostrate your
# abillity to programatically manipulate
# file content:
# Create the file ../../output/colon_delimited.txt,
# containing the contents of column 3 from
# the colon_delimited file ../../input/colon_delimited.txt.
# The output should be sorted in reverse order,
# and any duplicate rows should be removed

cat ../../output/colon_delimited.txt | awk -F':' '{ print $3 }' | sort -r | uniq >> ../../output/colon_delimited.txt

# Create the file ../../output/comma_delimited.txt,
# containing the contents of the 2nd and 3rd columns from
# the comma_delimited file ../../input/comma_delimited.txt.
# The output should remain comma-delimited and be sorted numerically,
# by the value in the third column in the default ascending order
cat input_comma_delimited.txt | awk -F',' '{ print $3","$2 }' | sort >> inter_file.txt
cat inter_file.txt | awk -F',' '{ print $2 "," $1 }' >> ../../output/comma_delimited.txt
rm inter_file.txt

# Create the file ../../output/user_rows.txt, containing any lines
# from ../../input/user_rows.txt which contain both of the whole words root
# and user in any order. Do not sort this ouput or maniulate it any further

egrep -w 'root|user' user_rows.txt  >> ../../output/user_rows.txt

# Create the file /opt/CECM00201/output/high_scores.txt ,
# containing any row from the colon-delimited
# file /opt/CECM00201/input/high_scores.txt ,
# where the first column contains
# an integer value greater than 100 .
# Sort this output in default ascending order, based upon the contents of the third column.

cat scores.txt | awk -F':' '$1>100' | awk -F':' '{ print $3 ":" $1 ":" $2}' | sort > intermedia_values.txt
cat intermedia_values.txt | awk -F':' '{print $2 ":" $3 ":" $1}' >> /opt/CECM00201/output/high_scores.txt

# Ejercicio 13
# For this item, you must ssh to the node net-exam-2 and complete all
# tasks on this node. Ensure that you return ti the base node (hostname: node-1)
# when you have complete this item
# Task
# To prevent access to a privileged system from an unauthorized
# account on your shared system, reject all traffic to host
# net-exam-2 on port 432/TCP from a local user charles
# persistently

sudo iptables -A OUTPUT -p tcp -m tcp --dport 432 -m owner --uid-owner {USERNAME} -j REJECT
sudo iptables -A OUTPUT -p tcp -d 192.168.100.207 --dport 432 -m owner --uid-owner {USERNAME} -j REJECT
sudo iptables -A OUTPUT -p tcp -d 127.0.0.1 --dport 432 -m owner --uid-owner {USERNAME} -j REJECT

# Ejercicio 14
# Any non-trivial work with source code or configuration files
# requires the use of a source code management tool, for
# auditabillity and rollbaack capabillities (among many other reasons),
# especially when working within a distributed team.

# Task
# Perform the following tasks to demostrate your abillity to perform
# basic source code management tasks with Git

# 1. Clone the repository /srv/repo/prodapp.git from remote server
# git-server using ssh. Appropiate ssh access has already been
# configured. Ensure that your cloned repository is available at
# /opt/CECM00301/work/prodapp
# 2. Create a new branch in your cloned repository, named bugfix,
# ane ensure it is checked out
# 3. Add the file /opt/CECM00301/data/newfile.txt to the new branch,
# and commit the new file
# 4. Push the new branch with the committed change to the origin
# remote
# 5. Determine the commit when the [DEBUG] stanza was added to the
# conf/foo.conf configuration file on the master branch of the
# prodapp repository, and write the full Git commit hash only
# of the appropriate commit to /opt/CECM00301/output/commit.txt


cd /opt/CECM00301/work
git clone ssh://git-server/srv/repo/prodapp.git
cd prodapp
git checkout -b bugfix
sudo cp /opt/CECM00301/data/newfile.txt newfile.txt
git add .
git commit -m "newfile.txt added"
git push ssh://git-server/srv/repo/prodapp.git
git checkout master
git log -p conf/foo.conf

# Ejercicio 15
# For this item, you must ssh to the nodesldap-server and
# ldap-client and complete tasks on these nodes. Ensure
# that you return to the base node (hostname: node-1)
# when you have completed this item

# Your organization has centralized authentication source
# running on the server ldap-server. This is a simple
# OpenLDAP implementation, and serves authentication request
# over ldaps. A new cliente machine, ldap-client, needs to be
# connected to shis centralized authentication server.

# Task
# Configure the node ldap-client so that ldap-server
# is used for any pam authentication attemps after the
# usual local password database has been consulted. A test user
# exists within the LDAP directory called testuser with password
# Passw0rd123

# Once your work is complete, you should be able to
#               ssh testuser@ldap-client
# from tha base node, and successfully authenticate against
# the directory available on ldap-server.
# Aditionally, you must ensure that users connecting
# via ssh for the fisrt time on ldap-client have their
# home directory automatically created.

# You can connect to both ldap-client and ldap-server as
# the root user with the following command:
#                 ssh root@<server>
# The Base DN is dc=auth,dc=example,dc=com. The Root DN
# password is Passw0rd123

# instalando el servidor ldap
# sudo apt-get install slapd ldap-utils
# revisar si el server esta corriendo
# sudo systemctl status slapd
# habilitar en el firewall
# sudo ufw allow ldap

# en el server ldap -- buscar si es que no esta el usuario
ldapsearch -x # usar este comando usuario testuser
ldapsearch -x # buscar por el root o el admin
#crear con useradd y passwd
#crear un grupo en ldapgroup

#o si le creo un grupo luego creo el archivo con el siguiente contenido

touch baseldapdomain.ldif

dn: dc=auth,dc=example,dc=com
objectClass: top
objectClass: dcObject
objectclass: organization
o: example com
dc: example

dn: cn=Manager,dc=auth,dc=example,dc=com
objectClass: organizationalRole
cn: Manager
description: Directory Manager

dn: ou=People,dc=auth,dc=example,dc=com
objectClass: organizationalUnit
ou: People

dn: ou=Group,dc=auth,dc=example,dc=com
objectClass: organizationalUnit
ou: Group

sudo ldapadd -Y EXTERNAL -x -D cn=Manager,dc=example,dc=com -W -f baseldapdomain.ldif

#despues esto

touch ldapgroup.ldif

dn: cn=Manager,ou=Group,dc=auth,dc=example,dc=com  # (1)
objectClass: top
objectClass: posixGroup
gidNumber: 1005 # este esl numero de grupo del usuario testUser

lo que tiene que tener es el gidNumber

sudo ldapadd -Y EXTERNAL -x -W -D "cn=Manager,dc=auth,dc=example,dc=com" -f ldapgroup.ldif # lo que esta en "" es lo de (1)

# despues creamos el archivo para el usuario
touch ldapuser.ldif

dn: uid=tecmint,ou=People,dc=auth,dc=example,dc=com
objectClass: top
objectClass: account
objectClass: posixAccount
objectClass: shadowAccount
cn: tecmint
uid: tecmint
uidNumber: 1005
gidNumber: 1005
homeDirectory: /home/tecmint
userPassword: {SSHA}PASSWORD_HERE
loginShell: /bin/bash
gecos: tecmint
shadowLastChange: 0
shadowMax: 0
shadowWarning: 0

sudo ldapadd -Y EXTERNAL -x -D cn=Manager,dc=auth,dc=example,dc=com -W -f ldapuser.ldif

#instalemos el cliente
sudo apt update && sudo apt install libnss-ldap libpam-ldap ldap-utils nscd
apt-get --reinstall libnss-ldap libpam-ldap ldap-utils nscd
y si no puedo
lo desinstalo y lo vuelvo a instalar

# me va a hacer un propmt pidiendo la url del tipo

ldapi:///auth.example.com

# luego pide el distinguished name seria asi
dc=auth,dc=example,dc=com

# pregunta make local root dabase admin poner si
# pregunta si LDAP database require login poner no

#pregunta por LDAP account for root (buscar en el server) es parecido a 
cn=manager,dc=auth,dc=example,dc=com

pide la contraseña que seria asi
Passw0rd123

#para modificar el archivo hay que hacerlo en /etc/ldap.conf

# configurar el perfile de LDAP por NSS corriendo

sudo auth-client-config -t nss -p lac_ldap

# despues actulizar pam
sudo pam-auth-update
y elegimos LDAP auth

para el directorio gome del ususario se cree automaticamente hacer
sudo vim /etc/pam.d/common-session

y agregar la linea

session required pam_mkhomedir.so skel=/etc/skel umask=077

#actualizar el servicio
sudo systemctl restart nscd
sudo systemctl enable nscd

si son muchos servidores ldap, en /etc/ldap.conf 
agregar al lado de uri, todos los servers que exitan

para testear si funciona hacer
getnet passwd testuser

# Ejercicio 16
# An application provide by a package installed on 
# the system has been flagged as containing a recently
# introduced security vulnerability, and as such,
# you have been instructed to downgrade package ceos00201, 
# currently at version 0.0.2, to the previous available version

# Perform the package downgrade

apt-cache showpkg ceos00201 # ---> esto tira la lista de versiones
sudo apt-get install ceos00201=«version» #---> la version previa a la 0.0.2

# Ejercicio 17
# Context
# An existing OpenVPN server shall be made more reliable
# and more secure. To keep clients connected, a new and
# improved instance shall run in parallel to the existing one.

# Task
# Reconfigure the existing instance
# 1. Reconfigure the existing instance to use port 21194
# 2. Restart the instance

# Only change the portm any other change might break the instance

# Configure a new instance
# 1. configure a new OpenVPN instance named
# newserver based on the existing configuration, but
# use port 1194
# use tcp instead of udp
# use a TSL.Auth key (do not se option key-direction)
# use cipher CAMELLIA-256-CBC

# 2. Make sure thee new instance is started and enabled

# Prepare a client configuration
# 1. Create a client configuration file
# /opt/CENW00202/client.config having all necessary certificates and
# keys inline

# Use the client certificate
# /root/easy-rsa/keys/client.crt and the key
# /root/easy-rsa/keys/client.key

#configuracion desde 0 del servidor de vpn
apt-get install openvpn easy-rsa
cp -R /usr/share/easy-rsa/ /etc/openvpn/
cp openssl-1.0.0.cnf openssl.cnf
source ./vars
./clean-all
./build-ca
./build-key-server server  #todo aceptar dejae en blanco el passwprd y el nombre opcional de la compañia, responder lo de firmar el certificado con Y
./build-dh

#generamos la key del cliente
./build-key client

cp -R keys/ /etc/openvpn/

#configuramos open vpn
cd /etc/openvpn/
nano server.conf

#contenido del archivo
#change with your port
port 1194

#You can use udp or tcp
proto udp

# "dev tun" will create a routed IP tunnel.
dev tun

cipher CAMELLIA-256-CBC  # camellia-256-cbc o lowercase

# Generate with:
#   openvpn --genkey tls-auth ta.key
#
# The server and each client must have
# a copy of this key.
# The second parameter should be '0'
# on the server and '1' on the clients.
tls-auth ta.key 0 # This file is secret

#Certificate Configuration

#ca certificate
ca /etc/openvpn/keys/ca.crt

#Server Certificate
cert /etc/openvpn/keys/server.crt

#Server Key and keep this is secret
key /etc/openvpn/keys/server.key

#See the size a dh key in /etc/openvpn/keys/
dh /etc/openvpn/keys/dh2048.pem

#Internal IP will get when already connect
server 10.1.1.0 255.255.255.0

#this line will redirect all traffic through our OpenVPN
push "redirect-gateway def1"

#Provide DNS servers to the client, you can use goolge DNS
push "dhcp-option DNS 8.8.8.8"
push "dhcp-option DNS 8.8.4.4"

#Enable multiple client to connect with same key
duplicate-cn

keepalive 20 60
comp-lzo
persist-key
persist-tun
#daemon

log-append /var/log/myvpn/openvpn.log

#Log Level
verb 3

#habilitamos ufw
ufw enable

#hacemos port fordwarding
sysctl -w net.ipv4.ip_forward=1

#reiniciamos el server
systemctl restart openvpn@server.service

#abrimos el puerto
ufw allow 1194/udp

# modificamos para UFW acepte forward
nano /etc/default/ufw
DEFAULT_FORWARD_POLICY="ACCEPT"

#agregamos unas modificaciones para que haga nat
nano /etc/ufw/before.rules

#
# rules.before
#
# Rules that should be run before the ufw command line added rules. Custom
# rules should be added to one of these chains:
#   ufw-before-input
#   ufw-before-output
#   ufw-before-forward
#

# START OPENVPN RULES
# NAT table rules
*nat
:POSTROUTING ACCEPT [0:0]
# Allow traffic from OpenVPN client to eth0
-A POSTROUTING -s 10.1.1.0/24 -o <tu_placa_de_red> -j MASQUERADE
COMMIT
# END OPENVPN RULES

# Don't delete these required lines, otherwise there will be errors
*filter

### creando conexion para el cliente
client
dev tun
proto udp

#Server IP and Port

cd /etc/openvpn/
nano client.ovpn # o tambien client.conf

remote 192.168.100.209 31194  #---> ip publica del server

resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
tls-auth ta.key 1

<ca>
(insert ca.crt here)
</ca>
<cert>
(insert client.crt here)
<cert>
<key>
(insert client.key here)
</key>

ns-cert-type server
comp-lzo

#Creando una nueva instancia
cd /etc/openvpn/
openvpn --genkey tls-auth ta.key
cp server.conf server2.conf
nano server2.conf

port 1194
proto tcp
server 10.1.2.0 255.255.255.0
cipher CAMELLIA-256-CBC  # camellia-256-cbc o lowercase

# agregar un nuevo port-forwarding
iptables -t nat -A POSTROUTING -s 10.1.2.0/24 -o enp0s8 -j MASQUERADE

#abrimos el nuevo puerto
ufw allow 1194/tcp

#agregamos la nueva regla a /etc/ufw/before.rules
nano /etc/ufw/before.rules

#tiene que quedar asi
#####
# START OPENVPN RULES
# NAT table rules
*nat
:POSTROUTING ACCEPT [0:0]
# Allow traffic from OpenVPN client to eth0
-A POSTROUTING -s 10.1.1.0/24 -o eth0 -j MASQUERADE
-A POSTROUTING -s 10.1.2.0/24 -o eth0 -j MASQUERADE
COMMIT
# END OPENVPN RULES

#iniciamos los servidores
systemctl start openvpn@server2.service
systemctl start openvpn@server.service

# Ejercicio 18
# Context
# Creating a customized distribution-specific package is
# a requeriment for any engineer. Software thar is only
# available as source code should be packaged using the
# distribution's package building system and installan and
# managed just as upstream packages would be.
# Task
# A basic source tarball is available at /srv/CESD00302/ced00302-0.0.1.tar.xz.
# You must use the distribution's package building system to create an appropriate package
# to install this software. The package mus be copied to the 
# /srv/CESD00302/outfiles directory when created, which is already 
# present on the system. Some files which may assist you in this process are also
# located in the /srv/CESD00302 directory. The package
# version must be 0.0.1, and the release/revision must be 1

# You should use the pkgbuild user, present on the system,
# for this task. The user's home directory has been populated with an 
# appropriate build directory structure

# You must not sing your package

# verificar que este instalado
su pkgbuild
sudo apt-get install dh-make fakeroot build-essential
cp /srv/CESD00302/ced00302-0.0.1.tar.xz /srv/CESD00302/outfiles/ced00302-0.0.1.tar.xz
cd /srv/CESD00302/outfiles/ 
tar xvf ced00302-0.0.1.tar.xz

#revisar que el Makefile tenga al menos esto
BIN = $(DESTDIR)/usr/bin
CFLAGS :=       -O $(CFLAGS)
TARGET=         myhello
SRCFILES=       myhello.c

all: $(TARGET)

$(TARGET):      $(SRCFILES) 
        $(CC) $(CFLAGS) -o $(TARGET) $(SRCFILES)

install: $(TARGET)
        install -d $(BIN)
        install $(TARGET) $(BIN)

clean:
        rm -f $(TARGET)

# build the package
# clean up from any previous attempts
rm -rf  WORK && mkdir WORK && cd WORK

cd myappdebian-1.0
dh_make -f ../*myappdebian-1.0.tar.gz
dpkg-buildpackage -uc -us
# check its contents
dpkg --contents ../*.deb

echo you should be able to install now with dpkg --install

# Ejercicio 19
# for this item, you mmust ssh to the nodw load-balancer
# and complete all tasks on this node. Ensure that you
# return to the base node (hostname: node-1) when you 
# have completed this item.

# Context
# Your company has recently set up new, redudant webservers
# to host an intranet sites

# Task
# Implement a reverse-proxying load-balancer on machine 
# load-balancer to load balance incoming request on port 8090 over
# load-balancer's eth0 interface to round-robin load balance across the
# backend hosts net-app-0:7644 and net-app-1:7644. The protocol for all
# request is TCP.

# Ensure that your solution is configured persistently so that it comes online
# automatically if load-balancer is rebooted.

# You should use haproxy or nginx for this task. Package repositories
# have been configured for you where needed. If you wish to use
# another solution, you are free to do so, but you'll need to configure
# any required package repositories youself

# solo configuro el entorno de pruebas
# docker run -d -p 8080:7645 --network=host initcron/vote 
# docker run -d -p 8080:7644 --network=host initcron/vote 

sudo apt-get install nginx
#intentar levantar el server y si no levanta copiar el vhost
sudo ln -s /etc/nginx/sites-available/vhost /etc/nginx/sites-enabled/vhost


#agregando load-balancer
vim /etc/nginx/conf.d/load-balancer.conf
http {
   upstream backend {
#no poner nada si queremos que sea round robin, para el resto ip_hash; hash $scheme$request_uri; least_conn; least_time (header | last_byte);
      server 192.168.100.209:80; 
      server 192.168.100.113:80;
   }

   # This server accepts all traffic to port 80 and passes it to the upstream. 
   # Notice that the upstream name and the proxy_pass need to match.

   server {
      listen 8080; 

      location / {
          proxy_pass http://backend;
      }
   }
}

#remover default
sudo rm /etc/nginx/sites-available/default
systemctl restart nginx

#si da error comentar toda la parte http del archivo nginx.conf 
events {
        worker_connections 768;
        # multi_accept on;
}

#http {

        ##
        # Basic Settings
        ##

#       sendfile on;
#       tcp_nopush on;
#       tcp_nodelay on;
#       keepalive_timeout 65;
#       types_hash_max_size 2048;
        # server_tokens off;

y agregar arriba el include
include /etc/nginx/conf.d/*.conf;

#y hacemos un restart
systemctl restart nginx
systemctl enable nginx

# Ejercicio 20
# Context
# The engineering team have filed a request for
# containerized infraestructure to be provisioned
# for a new application currently under development. 
# Their request comprises the following requirements, which
# you should implement accordingly:

# Task
#         - Start a new Docker container named webdev
#         using the nginx:1.15.0 image
#         - The container should start automatically
#         whenever the Docker daemon is restarted
#         or the host is rebooted
#         - The /srv/CESD00503/html/ directory on the host,
#         which contains the web development content, should
#         be mounted into the container at /var/www/htdocs.
#         The container's memory should ve contrained to 300m

docker container run --name webdev -m 300m --restart unless-stopped -v /srv/CESD00503/html:/var/www/htdocs nginx:1.15.0

